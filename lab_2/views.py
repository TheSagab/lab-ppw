from django.shortcuts import render
from lab_1.views import mhs_name, birth_date

#TODO Implement
#Create a content paragraph for your landing page:
landing_page_content = "Currently I am studying Computer Science at University of Indonesia. Feel free to browse around my website. It is not much, but hopefully I can improve my skill to create a better website."

def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)
