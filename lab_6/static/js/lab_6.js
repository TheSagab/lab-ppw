// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    print.value = "";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'log') {
      print.value = Math.round(Math.log10(print.value) * 10000) / 10000;
      erase = true;
  } else if (x === 'sin'){
      print.value = Math.round(Math.sin(print.value * Math.PI / 180) * 10000) / 10000;
      erase = true;
  } else if (x === 'tan'){
      print.value = Math.round(Math.tan(print.value * Math.PI / 180) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]

var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

$(document).ready(function() {
    $("#Arrow").click(function(){
        $(".chat-body").toggle();
        var src = ($(this).attr('src') === 'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png')? 
        'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_up-16.png' : 
        'https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png';
        $(this).attr('src', src);
  });
  // Chatbox
  $('textarea').keypress(function(event){
    if (event.which == 13 && !event.shiftKey) {
      var message = $(this).val();
      event.preventDefault();
      if (message.length == 0) {
        alert("Empty message");
      }
      else{
        $(".msg-insert").append("<p class='msg-send'>"+message+"</p>");
        $(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast'); //auto scroll to the latest chat
        $(this).val("");
      }
    }
  });
  localStorage.setItem('themes', JSON.stringify(themes));
  localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  var setTheme = JSON.parse(localStorage.getItem('selectedTheme'));
  $('body').css('background', setTheme.bcgColor);
  $('body').css('color', setTheme.fontColor);

  $('.my-select').select2({
    'data': JSON.parse(localStorage.getItem('themes'))
  })

  $('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var themeValue = $('.my-select').val();
    var setTheme = JSON.parse(localStorage.themes);
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    var findTheme;
    for (var i = 0; i < setTheme.length; i++) {
      if (themeValue == setTheme[i].id) {
        findTheme = setTheme[i];
        break;
      }
    }
    // [TODO] ambil object theme yang dipilih

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    $('body').css('background', findTheme.bcgColor);
    $('body').css('color', findTheme.fontColor);
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem('selectedTheme', JSON.stringify(findTheme));
  })
});
